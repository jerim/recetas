<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/familias', 'FamiliaController');
Route::resource('/recetas', 'RecetaController');
Route::get('/ingredientes/cook', 'IngredienteController@cook');
Route::get('/ingredientes/borrar', 'IngredienteController@borrar');
Route::get('/ingredientes/guardarCesta', 'IngredienteController@guardarCesta');
Route::get('/recetas/{id}/nombreReceta', 'RecetaController@nombreReceta');
Route::post('/ingredientes/recetas', 'IngredienteController@añadirCantidad');


Route::post('/ingredientes/pasos','IngredienteController@añadirPasos');
Route::resource('/ingredientes', 'IngredienteController');


//shows
//familia
Route::get('familias/{id}', function ($id) {
   return "Ver contenido de la familia $id";
});


Route::get('ingredientes/{id}', function ($id) {
   return "Ver contenido del ingrediente $id";
});



Route::get('recetas/{id}', function ($id) {
   return "Ver contenido del receta $id";
});
