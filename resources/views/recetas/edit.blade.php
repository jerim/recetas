@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Editar Receta</h1>
                            <form class="form"  method="post" action="/recetas/{{$receta->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="put">

                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input class="form-control" type="text" name="name" value="{{$receta->name}}">

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif

                                </div>


                                 <div class="form-group">
                                    <label>Tiempo</label>
                                    <input class="form-control" type="text" name="time" value="{{$receta->time}}">

                                    @if ($errors->first('time'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('time')}}
                                    </div>
                                    @endif

                                </div>


                                <div class="form-group">
                                    <label>Familia</label>
                                    <select class="form-control" type="text" name="family_id" >
                                        <option></option>
                                    @foreach($familias as $familia)
                                        <option value="{{$familia->id}}" {{$receta->family_id == $familia->id ? 'selected="selected"' : ' '}}>{{$familia->name}}</option>
                                    @endforeach
                                    </select>

                                     @if ($errors->first('family_id'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('family_id')}}
                                    </div>
                                    @endif


                                </div>




                                 <input type="submit" value="Editar Receta" class="btn btn-success"  role="button">
                         </form>
                </div>

            </div>
    </div>
@endsection


