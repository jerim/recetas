@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Editar Familia</h1>
                            <form class="form"  method="post" action="/familias/{{$familia->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="put">

                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input class="form-control" type="text" name="name" value="{{$familia->name}}">

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif

                                </div>



                                 <input type="submit" value="Editar Familia" class="btn btn-success"  role="button">
                         </form>
                </div>

            </div>
    </div>
@endsection


