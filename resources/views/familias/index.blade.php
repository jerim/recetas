@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Familias</h1>
        <a  href="/familias/create" class="btn btn-success"  role="button" >Crear</a>
      <table  class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Nombre</th>
            </tr>
        </thead>

        <tbody>
            @foreach($familias as $familia )

            <tr>
               <td>{{$familia->name}}</td>
               <td><a  href="/familias/<?php echo $familia->id ?>" class="btn btn-success"  role="button" >Ver</a></td>
                <td><a  href="/familias/<?php echo $familia->id ?>/edit" class="btn btn-success"  role="button" >Editar</a></td>
               <td>
                  <form method="post" action="/familias/{{$familia->id}}">
                      {{ csrf_field() }}

                      <input type="hidden" name="_method" value="delete">
                      <input type="submit" value="Destroy" class="btn btn-danger"  role="button">

                  </form>
              </td>


          </tr>

          @endforeach


      </tbody>
  </table>

</div>
</div>
</div>
@endsection
