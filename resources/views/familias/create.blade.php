@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Crear Familia</h1>
                            <form class="form"  method="post" action="/familias">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input class="form-control" type="text" name="name" value="{{old('name')}}">

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif

                                </div>



                                 <input type="submit" value="Nueva Familia" class="btn btn-success"  role="button">
                         </form>
                </div>

            </div>
    </div>
@endsection


