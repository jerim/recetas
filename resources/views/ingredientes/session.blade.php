@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Recetas Session</h1>
      @if(Session::has('receta'))

      Nombre de la receta : {{Session::get('receta')->name}}

      @endif
      <br><br>

      <h1>Ingredientes</h1>


      <a  href="/ingredientes/guardarCesta" class="btn btn-success"  role="button" >Guardar Receta</a>
      <a  href="/ingredientes/borrar" class="btn btn-danger"   role="button" >Borrar Receta</a>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Ingrediente</th>
            <th>Añadir</th>

          </tr>
        </thead>

        <tbody>
          @foreach($ingredientes as $ingrediente )

          <tr>
           <td>{{$ingrediente->name}}</td>



       </tr>

       @endforeach
     </tbody>
   </table>



   <h2>
    Añade cantidad de ingredientes
  </h2>
  <ul>
    <form class="form"  method="post" action="/ingredientes/recetas">
    {{ csrf_field() }}

    <div class="form-group">
     <label>Cantidad</label>
     <input class="form-control" type="number" name="quantity" value="{{old('quantity')}}">

     @if ($errors->first('quantity'))
     <div class="alert alert-danger ">
      {{$errors->first('quantity')}}
    </div>
    @endif

  </div>

  <input type="submit" value="Añadir" class="btn btn-success"  role="button">

</form>

</ul>


<h2>
    Añade los pasos de la receta
  </h2>
  <ul>
    <form class="form"  method="post" action="/ingredientes/pasos">
    {{ csrf_field() }}

    <div class="form-group">
     <label>Pasos</label>
     <input class="form-control" type="text" name="description" value="{{old('description')}}">

     @if ($errors->first('description'))
     <div class="alert alert-danger ">
      {{$errors->first('description')}}
    </div>
    @endif

  </div>

  <input type="submit" value="Añadir" class="btn btn-success"  role="button">

</form>

</ul>

</div>
</div>
</div>
@endsection
