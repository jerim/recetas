<?php

namespace App\Policies;

use App\User;
use App\Familia;
use Illuminate\Auth\Access\HandlesAuthorization;

class FamiliaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the familia.
     *
     * @param  \App\User  $user
     * @param  \App\Familia  $familia
     * @return mixed
     */
    public function view(User $user, Familia $familia)
    {
        return $user->id == 1 || $user->id == $familia->user_id;
    }

    public function manage(User $user) {
        return $user->id == 1;
    }


    /**
     * Determine whether the user can create familias.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the familia.
     *
     * @param  \App\User  $user
     * @param  \App\Familia  $familia
     * @return mixed
     */
    public function update(User $user, Familia $familia)
    {
        //
    }

    /**
     * Determine whether the user can delete the familia.
     *
     * @param  \App\User  $user
     * @param  \App\Familia  $familia
     * @return mixed
     */
    public function delete(User $user, Familia $familia)
    {
        //
    }

    /**
     * Determine whether the user can restore the familia.
     *
     * @param  \App\User  $user
     * @param  \App\Familia  $familia
     * @return mixed
     */
    public function restore(User $user, Familia $familia)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the familia.
     *
     * @param  \App\User  $user
     * @param  \App\Familia  $familia
     * @return mixed
     */
    public function forceDelete(User $user, Familia $familia)
    {
        //
    }
}
