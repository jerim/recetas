<?php

namespace App\Policies;

use App\User;
use App\Ingrediente;
use Illuminate\Auth\Access\HandlesAuthorization;

class IngredientePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the ingrediente.
     *
     * @param  \App\User  $user
     * @param  \App\Ingrediente  $ingrediente
     * @return mixed
     */
    public function view(User $user, Ingrediente $ingrediente)
    {
          return $user->id == 1 || $user->id == $ingrediente->user_id;
    }

    public function manage(User $user) {
        return $user->id == 1;
    }
    /**
     * Determine whether the user can create ingredientes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ingrediente.
     *
     * @param  \App\User  $user
     * @param  \App\Ingrediente  $ingrediente
     * @return mixed
     */
    public function update(User $user, Ingrediente $ingrediente)
    {
        //
    }

    /**
     * Determine whether the user can delete the ingrediente.
     *
     * @param  \App\User  $user
     * @param  \App\Ingrediente  $ingrediente
     * @return mixed
     */
    public function delete(User $user, Ingrediente $ingrediente)
    {
        //
    }

    /**
     * Determine whether the user can restore the ingrediente.
     *
     * @param  \App\User  $user
     * @param  \App\Ingrediente  $ingrediente
     * @return mixed
     */
    public function restore(User $user, Ingrediente $ingrediente)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ingrediente.
     *
     * @param  \App\User  $user
     * @param  \App\Ingrediente  $ingrediente
     * @return mixed
     */
    public function forceDelete(User $user, Ingrediente $ingrediente)
    {
        //
    }
}
