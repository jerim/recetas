<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $fillable =['name','user_id'];

    public function recetas() {
        return $this->hasMany(Receta::class);
    }

    public function user() {
      return  $this->belongsTo(User::class);
    }
}
