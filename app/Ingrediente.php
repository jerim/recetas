<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    protected $fillable =['name','user_id'];

    public function recetas() {
        return $this->belongsToMany(Receta::class)->withPivot('quantity');
    }

    public function pasos() {
        return $this->hasMany(Paso::class);
    }
     public function user() {
      return  $this->belongsTo(User::class);
    }

}
