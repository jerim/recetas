<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingrediente;
use App\Receta;
use Session;
use Auth;
class IngredienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
      $this->middleware('auth')->except('index','show');
    }
    public function index()
    {
     $user = \Auth::user();
     if($user->can('manage',Ingrediente::class)) {
       $ingredientes=Ingrediente::all();

     } else {
      $ingredientes = Ingrediente::where('user_id',$user->id)->get();
    }

    return view('ingredientes.index',['ingredientes'=>$ingredientes]);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ingredientes=Ingrediente::all();
      $this->authorize('view', $ingredientes);
      return view('ingredientes.create',['ingredientes'=>$ingredientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules=[
        'name' => 'required|max:255'  ,
        'user_id' => 'exists:users,id' ,
      ];

      $messages=[
       'required'=>'El nombre debe estar requerido',
       'max'=>'maximo 255 caracteres'

     ];

     $request->validate($rules, $messages);

     $ingrediente = new Ingrediente;
     $ingrediente->fill($request->all());
     $ingrediente->user_id = \Auth::user()->id;
     $ingrediente->save();



     return redirect('/ingredientes');
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $ingrediente=Ingrediente::find($id);
      return view('ingredientes.show', ['ingrediente'=>$ingrediente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $ingrediente=Ingrediente::find($id);
      $this->authorize('view', $ingrediente);
      return view('ingredientes.edit', ['ingrediente'=>$ingrediente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $rules=[
      'name' => 'required|max:255'  ,
      'user_id' => 'exists:users,id' ,
    ];

    $messages=[
     'required'=>'El nombre debe estar requerido',
     'max'=>'maximo 255 caracteres'

   ];

   $request->validate($rules, $messages);

   $ingrediente = Ingrediente::find($id);
   $ingrediente->fill($request->all());
   $ingrediente->user_id = \Auth::user()->id;
   $ingrediente->save();



   return redirect('/ingredientes');
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $ingrediente = Ingrediente::destroy($id);
     $this->authorize('view', $ingrediente);

        return back(); //vuelva a la pag
      }




      public function cook(request $request){

        if(Session::has('receta')){
          $receta = Session::get('receta');
          $ingredientes = Ingrediente::all();
          $request->session()->put('receta', $receta);
          return view('ingredientes.session',[ 'receta' => $receta,'ingredientes'=>$ingredientes]);
        }

      }




  public function añadirCantidad(Request $request){
    $id=$request->input('ingrediente');
    $ingredientes = Ingrediente::find($id);
    $quantity=$request->input('quantity');
    $ingredientes =Ingrediente::where('ingredientes',$quantity->$id)->get();
    $rules=[
     'quantity' =>'required|numeric|max:200',
    ];
    $messages = [
     'required' => 'The :attribute field is required.',
     'max' => 'The :attribute numeric is  max :200',
    ];
    $request->validate($rules,$messages);

    $ingredientes->quantity = $quantity;

    if ($request->session()->has('ingredientes')) {
      $ingredientes = $request->session()->get('ingredientes');
    } else {
      $ingredientes = [];
    }

    if(isset($ingredientes[$id])){
      // unset($ingredientes[$id]);
      //modificar cantidad???
      $ingredientes[$id]->quantity = $quantity;
    } else {
      $ingredientes[$id]=$ingredientes;
    }
    $request->session()->put('ingredientes', $ingredientes);

     return back();
 }



 public function añadirPasos(Request $request){
  $paso = $request->paso;
  $pasos=Session::get('pasos');

  if ($request->session()->has('pasos')) {
    $pasos = $request->session()->get('pasos');
  // } else {
  //   $pasos = [];
  }

  $pasos[] = $paso;
  $request->session()->put('pasos', $pasos);
}



public function plus($id){ //añadir question a la session (+)
  $question=Question::find($id);
  $questions=Session::get('questions');

  if(isset($questions[$question->id])){
    unset($questions[$id]);
  } else {
   $questions[$id]=$question;
  }
}



public function guardarCesta(Request $request){
  //1 crear receta

  //2 para cada paso, crear paso

  //3 para cada ingrediente hacer attach:
  //$recipe->ingredients()->attach($ingredient->id, ['quantity' => $ingredient->quantity])
}

public function borrar(Request $request){
 Session::forget('recetas');
 Session::forget('ingredientes');
  Session::forget('pasos');
 return redirect('/recetas');
}

}
