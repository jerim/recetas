<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Familia;
use Auth;
class FamiliaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $user = \Auth::user();
        if($user->can('manage',Familia::class)){
            $familias= Familia::all();

        }else{
            $familias = Familia::where('user_id',$user->id)->get();
        }

        return view('familias.index',['familias'=>$familias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $familias=Familia::all();
        $this->authorize('view', $familias);
        return view('familias.create',['familias'=>$familias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules=[
           'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];


         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

        $request->validate($rules,$messages);

        $familia = new Familia;
        $familia->fill($request->all());
        $familia->user_id = \Auth::user()->id;
        $familia->save();



      return redirect('/familias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $familia=Familia::find($id);
        return view('familias.show', ['familia'=>$familia]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $familia=Familia::find($id);
        $this->authorize('view', $familia);
        return view('familias.edit', ['familia'=>$familia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
        'name' => 'required|max:255'  ,
         'user_id' => 'exists:users,id' ,
        ];

        $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

        ];



        $request->validate($rules,$messages);

        $familia = Familia::find($id);
        $familia->user_id = \Auth::user()->id;
        $familia->fill($request->all());
        $familia->save();



      return redirect('/familias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $familia = Familia::destroy($id);
        $this->authorize('view', $familia);
        return back(); //vuelva a la pag
    }
}
