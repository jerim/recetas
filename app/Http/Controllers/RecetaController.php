<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Familia;
use App\Ingrediente;
use App\Paso;
use Auth;
use Session;

class RecetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $recetas = Receta::all();
        return view('recetas.index',['recetas'=>$recetas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $recetas = Receta::all();
        $familias = Familia::all();
        return view('recetas.create',['recetas'=>$recetas,'familias'=>$familias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
            'family_id' => 'exists:familias,id' ,
        ];

        $messages=[
         'required'=>'El nombre debe estar requerido',
         'max'=>'Máximo 255 caracteres',
         'exists'=>"Debe aparecer la familia_id"

     ];

     $request->validate($rules, $messages);

     $receta = new Receta;
     $receta->fill($request->all());
     $receta->user_id = \Auth::user()->id;
     $receta->save();



     return redirect('/recetas');
 }

  public function nombreReceta($id,Request $request){
        $receta=Receta::find($id);
        $request->session()->put('receta',$receta);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receta= Receta::find($id);

        return view('recetas.show',['receta'=>$receta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $receta= Receta::find($id);
        $familias = Familia::all();
        return view('recetas.edit',['receta'=>$receta,'familias'=>$familias]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
            'family_id' => 'exists:familias,id' ,
        ];

        $messages=[
         'required'=>'El nombre debe estar requerido',
         'max'=>'Máximo 255 caracteres',
         'exists'=>"Debe aparecer la familia_id"

     ];

     $request->validate($rules, $messages);

     $receta = Receta::find($id);
     $receta->fill($request->all());
     $receta->user_id = \Auth::user()->id;
     $receta->save();



     return redirect('/recetas');
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Receta::destroy($id);

        return back(); //vuelva a la pag
    }
}
