<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Familia;
use App\Receta;
class FamiliaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Familia::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $rules=[
           'name' => 'required|max:255'  ,
           'user_id' => 'exists:users,id' ,
         ];


         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $familia= new Familia;
        $familia->fill($request->all());
        $familia->save();

        return $familia;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $familia=Familia::with('user')->find($id);
        if($familia){
            return $familia;
        }else{
            return response()->json([
                'message'=>"Lista no encontrada",
            ],400);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
           'name' => 'required|max:255'  ,
           'user_id' => 'exists:users,id' ,
         ];


         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $familia=Familia::with('user')->find($id);

        if(!$familia){
            return response()->json([
                 'message'=>"familia no actualizada ni encontrada",
            ],404);
        }

        $familia->fill($request->all());
        $familia->save();
        $familia->refresh();

        return $familia;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Familia::destroy($id);
        return response()->json([
            'message'=>'Familia destruida',
        ],201);
    }
}
