<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Ingrediente;
class IngredienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return Ingrediente::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $ingrediente= new Ingrediente;
        $ingrediente->fill($request->all());
        $ingrediente->save();

        return $ingrediente;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingrediente=Ingrediente::with('user')->find($id);
        if($ingrediente){
            return $ingrediente;
        }else{
            return response()->json([
                'message'=>"Ingrediente no encontrado",
            ],400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name' => 'required|max:255'  ,
            'user_id' => 'exists:users,id' ,
         ];

         $messages=[
           'required'=>'El nombre debe estar requerido',
           'max'=>'maximo 255 caracteres'

         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $ingrediente=Ingrediente::with('user')->find($id);

        if(!$ingrediente){
            return response()->json([
                 'message'=>"ingrediente no actualizado ni encontrado",
            ],404);
        }

        $ingrediente->fill($request->all());
        $ingrediente->save();
        $ingrediente->refresh();

        return $ingrediente;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ingrediente::destroy($id);
        return response()->json([
            'message'=>'Ingrediente destruido',
        ],201);
    }
}
