<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $fillable =['name','time','user_id','family_id'];

    public function ingredientes() {
           return $this->belongsToMany(Ingrediente::class)->withPivot('quantity');
    }



    public function familias() {
        return $this->belongsTo(Familia::class);
    }

    public function pasos() {
        return $this->hasMany(Paso::class);
    }

    public function user() {
      return  $this->belongsTo(User::class);
    }

}
