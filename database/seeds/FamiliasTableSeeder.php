<?php

use Illuminate\Database\Seeder;

class FamiliasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('familias')->insert([
            'name' => 'Pasta',
        ]);
        DB::table('familias')->insert([
            'name' => 'Carnes',
        ]);
        DB::table('familias')->insert([
            'name' => 'Pescados',
        ]);
        DB::table('familias')->insert([
            'name' => 'Verduras',
        ]);
        DB::table('familias')->insert([
            'name' => 'Postres',
        ]);
        DB::table('familias')->insert([
            'name' => 'Bebidas',
        ]);
        DB::table('familias')->insert([
            'name' => 'Reposteria',
        ]);
        DB::table('familias')->insert([
            'name' => 'Sopas',
        ]);
         DB::table('familias')->insert([
            'name' => 'Risotos',
        ]);
        DB::table('familias')->insert([
            'name' => 'Salados',
        ]);

    }
}
