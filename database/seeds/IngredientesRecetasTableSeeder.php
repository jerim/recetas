<?php

use Illuminate\Database\Seeder;

class IngredientesRecetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quantity=1;
        $ingrediente=\App\Ingrediente::find(1);
        $ingrediente->recetas()->attach(1, ['quantity' => $quantity]);

        $ingrediente=\App\Ingrediente::find(1);
        $ingrediente->recetas()->attach(2, ['quantity' => $quantity]);

        $ingrediente=\App\Ingrediente::find(1);
        $ingrediente->recetas()->attach(3, ['quantity' => $quantity]);
    }
}
